from django.contrib import admin
from core.forms import UrlAdminForm
from core.models import Url


class UrlAdmin(admin.ModelAdmin):
    form = UrlAdminForm
    readonly_fields = ('status',)

admin.site.register(Url, UrlAdmin)