#!-*-coding:utf-8-*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import TextInput
from django.utils.dateparse import parse_duration

from core.models import Url

__author__ = 'virgo'


class DurationInput(TextInput):

    def _format_value(self, value):
        try:
            duration = parse_duration(value)
            seconds = duration.seconds
            minutes = seconds // 60
            seconds = seconds % 60
        except AttributeError, ex:
            return value
        return '{:02d}:{:02d}'.format(minutes, seconds)


class UrlAdminForm(forms.ModelForm):
    timeshift = forms.DurationField(label=_(u'Сдвиг времени (mm:ss)'),
                                    widget=DurationInput)

    class Meta:
        model = Url
        fields = ('url', 'timeshift')