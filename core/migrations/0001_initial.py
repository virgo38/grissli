# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Url',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(default=b'http://127.0.0.1:8000/admin/', verbose_name='url')),
                ('timeshift', models.DurationField(verbose_name='\u0421\u0434\u0432\u0438\u0433 \u0432\u043e \u0432\u0440\u0435\u043c\u0435\u043d\u0438 (hh:mm:ss)')),
                ('title', models.CharField(max_length=256, blank=True)),
                ('coding', models.CharField(max_length=64, blank=True)),
                ('h1', models.CharField(max_length=1024, blank=True)),
                ('status', models.CharField(default=b'NW', max_length=2, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(b'NW', b'new'), (b'PR', b'processing'), (b'FN', b'finish')])),
                ('success', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='UrlData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('coding', models.CharField(max_length=64)),
                ('h1', models.CharField(max_length=1024)),
                ('url', models.ForeignKey(to='core.Url')),
            ],
        ),
    ]
