# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='url',
            options={'ordering': ['timeshift']},
        ),
        migrations.AddField(
            model_name='url',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 29, 4, 26, 48, 216652, tzinfo=utc), verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='url',
            name='coding',
            field=models.CharField(max_length=64, verbose_name='\u041a\u043e\u0434\u0438\u0440\u043e\u0432\u043a\u0430 \u0441\u0442\u0440.', blank=True),
        ),
        migrations.AlterField(
            model_name='url',
            name='h1',
            field=models.CharField(max_length=1024, verbose_name='\u0422\u044d\u0433\u0438 h1 \u0441\u0442\u0440.', blank=True),
        ),
        migrations.AlterField(
            model_name='url',
            name='success',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442'),
        ),
        migrations.AlterField(
            model_name='url',
            name='title',
            field=models.CharField(max_length=256, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440.', blank=True),
        ),
    ]
