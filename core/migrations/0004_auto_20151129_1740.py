# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_url_time_processing'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='url',
            options={'ordering': ['time_finish']},
        ),
        migrations.AddField(
            model_name='url',
            name='time_finish',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 29, 17, 40, 26, 117480, tzinfo=utc), verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0438 url'),
            preserve_default=False,
        ),
    ]
