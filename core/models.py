#-*-coding:utf-8-*-
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Url(models.Model):
    NEW = 'NW'
    PROCESSING = 'PR'
    FINISH = 'FN'
    STATUSES = (
        (NEW, 'new'),
        (PROCESSING, 'processing'),
        (FINISH, 'finish'),
    )

    url = models.URLField(_(u'url'), default='http://127.0.0.1:8000/admin/')
    timeshift = models.DurationField(_(u'Сдвиг во времени (hh:mm:ss)'))
    title = models.CharField(_(u'Заголовок стр.'), max_length=256, blank=True)
    coding = models.CharField(_(u'Кодировка стр.'), max_length=64, blank=True)
    h1 = models.CharField(_(u'Тэги h1 стр.'), max_length=1024, blank=True)
    status = models.CharField(_(u'Статус'),  max_length=2, choices=STATUSES,
                              default=NEW)
    success = models.BooleanField(_(u'Результат'), blank=True, default=False)
    created = models.DateTimeField(_(u'Дата создания'), auto_now_add=True)
    time_processing = models.DateTimeField(_(u'Дата и время обработки url'))
    time_finish = models.DateTimeField(_(u'Дата и время окончания '
                                         u'обработки url'), blank=True,
                                       null=True)

    class Meta:
        ordering = ['time_finish']

    def __unicode__(self):
        return '[%s] %s...' % (self.id, self.url[:15])

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.time_processing = timezone.now() + self.timeshift
        super(Url, self).save(force_insert, force_update, using, update_fields)