# -*- coding:utf-8 -*-
import Queue
import threading
import time
import HTMLParser
import os
import json

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "grissli.settings")

from bs4 import BeautifulSoup
from requests.exceptions import ConnectionError, HTTPError, Timeout
import requests
from tornado import gen
import tornado.websocket
import tornado.ioloop
from tornado.ioloop import IOLoop
import tornado.web
from tornado.ioloop import PeriodicCallback
import tornado.httpserver
from django.utils import timezone
from django.core.serializers.json import DjangoJSONEncoder

from core.models import Url

PORT = 8888
THREADS_COUNT = 3
QUERY_PERIOD = 3  # sec


def _processing_url(url):
    """
    Парсит страничку по URL.
    :param url: адрес странички
    :return: dict {success: True/False, 'title': title,
    'coding': coding, 'h1': html of h1 tags}
    """
    data = {}.fromkeys(('title', 'coding', 'h1', 'success',), '')
    try:
        page = requests.get(url)
    except (ConnectionError, HTTPError, Timeout), ex:
        data['success'] = False
        print ex.message
        return data

    if page.status_code != 200:
        data['success'] = False
        print "Bad status code [%s]" % (
            page.status_code)
        return data

    try:
        soup = BeautifulSoup(page.content, 'html.parser')
    except HTMLParser.HTMLParseError, ex:
        data['success'] = False
        print ex.message
        return data

    # Find title
    data['title'] = soup.title.text if soup.title else ''
    # Find coding
    if soup.meta and soup.meta.attrs.get('charset'):
        data['coding'] = soup.meta.attrs.get('charset')
    # Find h1
    data['h1'] = str(soup.find_all('h1'))
    data['success'] = True
    return data


def update_url(url):
    """
    Делаем update экземпляра модели Url
    :param url: Url
    :return:
    """
    print 'Parsing %s...' % url.url
    data = _processing_url(url.url)
    print 'Parsing data of %s: %s' % (url.url, data)
    for key in data:
        url.__setattr__(key, data[key])
    url.status = Url.FINISH
    url.time_finish = timezone.now()
    url.save()
    print 'Save data of %s' % url.url


def worker(queue):
    """
    Обработка url.
    :param queue: очередь
    :return:
    """
    while True:
        item = queue.get()
        url = item[1]
        print 'Processing %s. Queue %s ' % (
            url.url, ','.join([x[1].url for x in queue.__dict__['queue']]))
        # Обрабатываем url
        update_url(url)
        print 'Processing %s is done' % url.url
        queue.task_done()


@gen.engine
def task_func():
    """
    Асинхронная функция для получения данных и постановки их в очередь
    для обработки.
    :return:
    """
    q = Queue.PriorityQueue()
    # Создаем и запускаем потоки обслуживающие очередь
    for i in range(THREADS_COUNT):
        t = threading.Thread(target=worker, args=(q,))
        t.setDaemon(True)
        t.start()

    while True:
        # Забираем из БД только url в статусе NEW и с
        # датой обработки меньше или равной текущей
        urls = Url.objects.filter(status=Url.NEW,
                                  time_processing__lte=timezone.now())
        if urls.count() == 0:
            loop = IOLoop.instance()
            yield gen.Task(loop.add_timeout, time.time() + QUERY_PERIOD)
            continue

        print 'Query to DB. Get urls count: %s' % urls.count()

        # Добавляем url в приор. очередь
        for url in urls:
            q.put((url.time_processing, url))

        # Обозначаем, что url находятся в статусе обработки
        urls.update(status=Url.PROCESSING)
        loop = IOLoop.instance()
        yield gen.Task(loop.add_timeout, time.time() + QUERY_PERIOD)


class EchoWebSocket(tornado.websocket.WebSocketHandler):
    time_finish = None
    keys = ('url', 'title', 'coding', 'h1', 'success',
                'time_processing', 'id')

    def open(self):
        print("WebSocket opened")
        urls = Url.objects.filter(status=Url.FINISH)
        self._send_json_msg(urls)

    def on_close(self):
        print("WebSocket closed")

    def check_origin(self, origin):
        return True

    def on_message(self, message):
        if self.time_finish:
            urls = Url.objects.filter(status=Url.FINISH,
                                      time_finish__gt=self.time_finish)
        else:
            urls = Url.objects.filter(status=Url.FINISH)
        self._send_json_msg(urls)

    def _send_json_msg(self, urls):
        if urls.exists():
            self.time_finish = urls.last().time_finish

        json_data = json.dumps(list(urls.values(*self.keys)),
                               cls=DjangoJSONEncoder)
        time.sleep(QUERY_PERIOD)
        self.write_message(json_data)

application = tornado.web.Application([
    (r'/websocket', EchoWebSocket),
])

if __name__ == "__main__":
    application.listen(PORT)
    ioloop = tornado.ioloop.IOLoop.instance()
    task_func()
    ioloop.start()
