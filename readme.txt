=====Задание=====.
1. Создать Django приложение в котором можно добавлять URL’s в административном
разделе (/admin/). С возможностью указать сдвиг во времени (минуты, секунды) через
сколько какой URL когда будет обработан (timeshift).
• Frontend
• Сделать два окна textarea.
• В первое окно выводить информацию об успехе или ошибке обработки URL’a (для
backend можно воспользоваться очередями Queue).
• Во второе вывести данные полученные из пункта 2.
2. Создать сервер для парсинга сайтов по URL’ам указанным в базе Django приложения
(п. 1), в указанный сдвиг времени или сразу запускать если не указан сдвиг.
• На сайтах получить
• Заголовок (title)
• Определить кодировку страницы
• Если есть, найти и получить H1
• Вывести данные во второе окно.

===Основные моменты:===
I. Сервер для обработки URL может быть выполнен в виде команды, либо в виде
полноценного сервера с собственными процессами/нитями (process/thread).
II. Данные по обработанным URL’ам сохранять в базу и при перезагрузке выводить их во
втором textarea.
III. Все сообщения в первом окне должны начинаться с даты и двоеточия
“<дата dd.mm.yyyy HH:mm:ss>: ”.
IV. Все сообщения во втором окне начинаются с URL и тире (dash), вида “http://example.com
- ”, с последующим перечислением что получено и чего нет. Если получить ничего не
удалось - тогда просто URL.
V. Запрещается использовать любые брокеры сообщений которым нужны собственные
серверные платформы (RabbitMQ, ActiveMQ и т.д.)
VI. Рабочий сайт должен запускаться из командной строки одной командой, вида “python
manage.py runserver” или “python run.py” или “python -m module -c command”, либо
bash/sh скриптом.
VII. Все зависимости должны устанавливаться из файла “requirements.txt”, который будет
лежать в корне кодового репозитория (зависимости которых нет в Pypi, указывать в
“http[s]://“ формате).
VIII. Описать процесс запуска кода на локальной машине
IX. Код должен запустится и работать на машине на которой установлен только python и
сопутствующие библиотеки, SQLite.

3. Написать SQL запрос в базу (один запрос) который выберет данные из таблиц 1,2,3 и
запишет в таблицу 4
Результат лежит в папке sql:
sql_test.bd - БД c таблицами
query.sql - там, собственно, сам запрос.


=====Подготовка=====
1. Установить из config/requirements.txt библиотеки
pip install -r config/requirements
2. Сделать файл run.sh испольняемым (chmod +x run.py).


=====Запуск и остановка=====
1. Перейти в корень проекта. Запустить сервер.
./run.sh
2. После запуска в консоле отобразиться сообщение:
"Background tasks are running in process group $pgid, kill with kill -TERM -$pgid"
Для остановки сервера нажать ctr+c или ввести команду в консоль
kill -TERM -$pgid


=====Архитектура=====
1. Добавление url в Django части (core).
2. Обработка url и общение с клиентов реализовано на сервере Tornado (он
асинхронен и хорошо подходит для данной задачи) в двух потоках.
2.1 Для многопоточной обработки url используются очереди Queue
(core/tornadoapp.py).
2.2 Передача данных клиенту реализована через WebSocket.
На сервере: tornado.websocket.WebSocketHandler (core/tornadoapp.py)
На клиенте: js - WebSocket (core/templates/core/index.html).
3. Для запуска Tornado используется ./manage.py start_tornado
(core/management/commands/start_tornado.py)
4. Для запуска одной командой написан bash скрипт ./run.sh


=====Возможные пути улучшения проекта=====
1. Для уменьшения запросов к БД прикрутить redis.
Через него реализовать общение Django и Tornado, а также использовать как кэш,
при отправке сообщений клиенту об результате обработки url.

