#!/bin/bash
setsid sh -c 'python manage.py runserver & python manage.py start_tornado &' &
pgid=$!
echo "Background tasks are running in process group $pgid, kill with kill -TERM -$pgid"
