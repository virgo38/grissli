INSERT INTO table4
(InternalNumber, "Name/Surname", Position, "Salary/Month", Tax, Month)
  select InternalNumber,  (Name ||' '||Surname), Position,
    ("Salary/year" / 12), taxes as 'Tax', Month
  from table2 as t2
    left join table3 as t3 on t1.id = t3.EmployeeID
    join table1 as t1 on t1.id = t2.EmployeeID;